// Copyright 2020-2021, Collabora, Ltd.
// SPDX-License-Identifier: BSL-1.0
package org.khronos.openxr.runtime_broker.utils

import android.content.ContentUris
import android.net.Uri
import android.os.Build
import android.provider.BaseColumns
import java.lang.IllegalArgumentException

object BrokerContract {
    /** The URI authority for the user-preference-controlled installable broker. */
    const val AUTHORITY = "org.khronos.openxr.runtime_broker"

    /** The URI authority for the system/vendor-provided broker. */
    const val SYSTEM_AUTHORITY = "org.khronos.openxr.system_runtime_broker"

    // path components used by multiple table types.
    const val BASE_PATH = "openxr"
    const val ABI_PATH = "abi"
    const val RUNTIMES_PATH = "runtimes"
    const val CONTENT_SCHEME = "content"

    fun brokerToAuthority(brokerType: BrokerType?): String {
        return when (brokerType) {
            BrokerType.RuntimeBroker -> AUTHORITY
            BrokerType.SystemRuntimeBroker -> SYSTEM_AUTHORITY
            else -> throw IllegalArgumentException()
        }
    }

    enum class BrokerType {
        RuntimeBroker,
        SystemRuntimeBroker
    }

    /**
     * Contains details for the /openxr/[major_ver]/abi/[abi]/runtimes/active URI.
     *
     * This URI represents a "table" containing items corresponding to the currently active runtime.
     * Older versions of the OpenXR loader only look at the first item, later items may be provided
     * for forward compatibility.
     *
     * The policy of which runtime is chosen to be active (if more than one is installed) is left to
     * the content provider.
     *
     * No sort order is required to be honored by the content provider.
     */
    object ActiveRuntime {
        /** Final path component to this URI. */
        const val TABLE_PATH = "active"

        /**
         * Create a content URI for querying the data on the active runtime for a given major
         * version of OpenXR.
         *
         * @param brokerType The broker type (regular/installable or system) to retrieve paths for
         * @param majorVer The major version of OpenXR.
         * @param abi The Android ABI to retrieve paths for
         * @return A content URI for a single item: the active runtime.
         */
        @JvmStatic
        fun makeContentUri(brokerType: BrokerType?, majorVer: Int, abi: String?): Uri {
            val builder = Uri.Builder()
            builder
                .scheme(CONTENT_SCHEME)
                .authority(brokerToAuthority(brokerType))
                .appendPath(BASE_PATH)
                .appendPath(majorVer.toString())
                .appendPath(ABI_PATH)
                .appendPath(abi ?: Build.SUPPORTED_ABIS[0])
                .appendPath(RUNTIMES_PATH)
                .appendPath(TABLE_PATH)
            ContentUris.appendId(builder, 0)
            return builder.build()
        }

        /** Contains the constants used for provider response columns in the /active URI. */
        object Columns : BaseColumns {
            /** Constant for the PACKAGE_NAME column name containing the Android package name */
            const val PACKAGE_NAME = "package_name"

            /**
             * Constant for the NATIVE_LIB_DIR column name, containing the ABI-specific absolute
             * path to the directory containing the dynamic library to load.
             */
            const val NATIVE_LIB_DIR = "native_lib_dir"

            /**
             * Constant for the SO_FILENAME column name, containing the filename of the dynamic
             * library to load.
             */
            const val SO_FILENAME = "so_filename"

            /**
             * Constant for the HAS_FUNCTIONS column name.
             *
             * If this column contains true, you should check the /functions/ URI for that runtime.
             */
            const val HAS_FUNCTIONS = "has_functions"
        }
    }

    /**
     * Contains details for the /openxr/[major_ver]/abi/[abi]/runtimes/[package]/functions URI.
     *
     * This URI is for package-specific function name remapping. Since this is an optional field in
     * the corresponding JSON manifests for OpenXR, it is optional here as well. If the active
     * runtime contains "true" in its "has_functions" column, then this table must exist and be
     * queryable.
     *
     * No sort order is required to be honored by the content provider.
     */
    object Functions {
        /** Final path component to this URI. */
        const val TABLE_PATH = "functions"

        /**
         * Create a content URI for querying all rows of the function remapping data for a given
         * runtime package and major version of OpenXR.
         *
         * @param brokerType The broker type (regular/installable or system) to retrieve paths for
         * @param majorVer The major version of OpenXR.
         * @param packageName The package name of the runtime.
         * @param abi The ABI to query.
         * @return A content URI for the entire table: the function remapping for that runtime.
         */
        @JvmStatic
        fun makeContentUri(
            brokerType: BrokerType?,
            majorVer: Int,
            packageName: String?,
            abi: String?
        ): Uri {
            val builder = Uri.Builder()
            builder
                .scheme(CONTENT_SCHEME)
                .authority(brokerToAuthority(brokerType))
                .appendPath(BASE_PATH)
                .appendPath(majorVer.toString())
                .appendPath(ABI_PATH)
                .appendPath(abi ?: Build.SUPPORTED_ABIS[0])
                .appendPath(RUNTIMES_PATH)
                .appendPath(packageName)
                .appendPath(TABLE_PATH)
            return builder.build()
        }

        /** Contains the constants used for provider response columns in the .../functions URI. */
        object Columns : BaseColumns {
            /**
             * Constant for the FUNCTION_NAME column name, containing the function name as found in
             * the specification documents.
             */
            const val FUNCTION_NAME = "function_name"

            /**
             * Constant for the SYMBOL_NAME column name, containing the symbol to load from the
             * dynamic library.
             */
            const val SYMBOL_NAME = "symbol_name"
        }
    }
}
